# UCSB Geography Windows Imaging Script #
## Version 0.1 ##


# README #

This README documents the steps necessary to push an image, create a new image, and update/modify the scripts and images.  This deployment process allows us to maintain a single baseline "Golden Image" with all of our latest site-licensed software, Windows Updates, etc.... as a starting point for new Windows 7 deployments instead of a fresh Win 7 SP*1-2-whatever* install with nothing on it.


### What is this repository for? ###

This repository provides as a public documentation and code repository for the people who use the tool.  The scripts are open source and -- given a valid Microsoft license and an understanding of the Microsoft Deployment Toolkit -- could be re-used to speed up the Windows deployment process.


## Step-By-Step Instructions ##

#### **Pushing an Image to a computer** ####

1.  Ensure the drive you want to be imaged is connected to SATA0 on the motherboard as well as configured as the first hdd/boot device in the BIOS.  
   > This is VERY important as the script assumes disk0 is the correct drive and formats/partitions the drive without warning
2.  Connect both the Windows PE USB and the drive containing the Windows Image (myimage.wim) into the computer to be imaged.
3.  Turn on the computer and boot to the Windows PE USB device
4.  The script will initialize and display the current volume list on the PC -- identify and remember the drive letter of the volume containing the Windows Image (myimage.wim).  It should be labeled "Image Files" or similar.
5.  You can the push image process by running `X:\PushImage.bat`
6.  You will be prompted to enter the drive letter for the volume containing the Windows Image (from step 4)
```
#!txt
   Enter only the letter: A   <enter>
                          B   <enter>
                          Z   <enter>
                          Y   <enter>
                          V   <enter>      etc....
```
```
#!raw
   Do NOT enter the drive letter with any punctuation or anything:
                          A:\  <---WRONG
                          B:   <---WRONG
                          Z:\  <---WRONG   etc...
```
##### Let the script run -- you should see a message saying the "**Boot files successfully created**" if the process completed successfully.

```
#!raw
IF YOU SEE THE MESSAGE "Failure when attempting to copy boot files"
AT THE END OF THE IMAGING PROCESS, RESTARTING THE IMAGE WILL NOT
WORK AND YOU NEED TO MANUALLY FINISH THE PROCESS (SEE STEPS BELOW)
```


#### **Updating the Golden Image File / Creating an Image File** ####




```
*Coming Soon*     *Coming Soon*    *Coming Soon*     *Coming Soon*    

     *Coming Soon*      *Coming Soon*     *Coming Soon*    
  
*Coming Soon*     *Coming Soon*    *Coming Soon*     *Coming Soon*      
```




------

## Advanced Configuration / Setup Instructions ##

##### *This section is still being written* #####

#### Summary of set up ####

Before these scripts can be used, a bootable WinPE USB/CD must be created using the Microsoft Deployment Toolkit.  I based these scripts on the documentation Microsoft provides for how to deploy Windows 7 and my intention was to make the process more "automated".  It is good to understand what is going on in case something breaks during the imaging process -- you can always figure out what happened and run the rest of the commands manually instead of starting from scratch.  I am working to make these tools as automated as possible and so please report any issues directly to bryan@geog dot ucsb.edu

The scripts are strictly based on the Microsoft Deployment Toolkit and the Windows Preinstallation Environment documentation.  My intention was to purely automated the process since, it was fairly long to do manually every time and (as Microsoft probably intended anyways) can be scripted.

These are the 2 steps to create the bootable WinPE USB/CD and the image file to be pushed.
  * Create the bootable WinPE USB/CD
  * Creating the Image (with Sysprep)





```
*Coming Soon*     *Coming Soon*    *Coming Soon*     *Coming Soon*    

     *Coming Soon*      *Coming Soon*     *Coming Soon*    
  
*Coming Soon*     *Coming Soon*    *Coming Soon*     *Coming Soon*      
```

#### Tweaks/Additions to our Windows PE Image that are not included in this repository.

##### These edits are required for the `ucsb-geog-winimager-script` to work out-of-the-box when copied to an unmodified Windows PE Image.

> **1. Modified `Windows\System32\startnet.cmd` to auto-launch the startup script (`startup-cmd.bat`).**

> *** Windows\System32\startnet.cmd :***
     
```
#!bat
      wpeinit
      CALL X:\ucsb-geog-winimager-script\resources\startup-cmd.bat
``` 
   
   
> **2. Created `PushImage.bat` at root of Windows PE image directory which calls the push script in `ucsb-geog-winimager-script`.  **
> This makes it easier to run the command from prompt (simply type X:\PushImage.bat instead of X:\ucsb-geog-winimager-script\scripts\PushWindowsImage.bat) and makes it easier to update the scripts since they are being referenced instead of called directly (can replace `ucsb-geog-winimager-script/` folder without modifying any other files in the WinPE Image).

> *** PushImage.bat :***
```
#!bat
     @ECHO OFF
     CALL X:\ucsb-geog-winimager-script\scripts\PushWindowsImage.bat
``` 
   
   
> **3. Added `imagex.exe` and `findstr.exe` to `Windows\System32` **
> These tools are used by these scripts.  They were copied from an existing Windows 7 install (from the `C:\Windows\System32\` directory)
  
  
-----

### How do I mount and edit the Windows PE Image or the `ucsb-geog-winimager-script/` folder on the Windows PE bootable USB? ###

1.  Make sure you have the Windows Automated Installation Kit (AIK) installed -- you need this to use the Deployment and Imaging tools Environment and work with the Windows Image files (.wim).

2.  Copy `boot.wim` from the `sources/` folder on the Windows PE USB.  In this example, I copied the file to `C:\temp`  (`C:\temp\boot.wim`)

3.  Create a directory next to `boot.wim` named `mount`

4.  Open the `Deployment and Imaging Tools Environment` as an **Administrator**

5.  Mount the Windows Image file (`C:\temp\boot.wim`) to the mount folder you just created (`C:\temp\mount\`) using Deployment Image Servicing and Management (DISM.exe):

```
#!dos
  > dism /Mount-Image /ImageFile:"C:\temp\boot.wim" /index:1 /MountDir:"C:\temp\mount"
```

##### This will mount the Windows Image and "extract" it's contents to `C:\temp\mount` so it is editable.

### ***From here you can make any changes you need.. including:***

##### * Updating the ucsb-geog-winimager-script from this repository: *

`git clone` or download the latest version from the [ucsb-geog-winimager-script repo](https://bitbucket.org/bryankaraffa/ucsb-geog-winimager-script) and replace the existing `ucsb-geog-winimager-script/` directory with the new one

##### * Making modifications to the ucsb-geog-winimager-script itself and pushing updates to the git repo: * 

You can edit files in the `ucsb-geog-winimager-script/` directory as normal and use git to push your changes to this code repo.  If you plan on doing this you will need a BitBucket account and access to push code to this repository -- you can contact Bryan or UCSB Geography IT to get this access if needed.

-----

### How do I save my changes to the mounted Windows Image and update the Windows PE USB?

> To commit the changes and unmount the Windows Image (you're all done with everything)
```
#!dos
  > Dism /Unmount-Image /MountDir:"C:\temp\mount" /commit
```

> You can discard the changes with this command:
```
#!dos
  > Dism /Unmount-Image /MountDir:"C:\temp\mount" /discard
```

> Or, you can commit the changes without unmounting the image with this command (useful for testing and developing):
```
#!dos
  > Dism /Commit-Wim /MountDir:"C:\temp\mount"
```

> *** For more information on how to mount and work with Windows Images, please reference these pages: ***

* [Deployment Image Servicing and Management (DISM) Command-Line Options](http://technet.microsoft.com/en-us/library/dd744382(v=ws.10).aspx)
* [WinPE: Mount and Customize](http://technet.microsoft.com/en-us/library/hh824972.aspx)

** Once you have committed/unmounted the Windows Image, you can copy the updated `boot.wim` (should be same as the /ImageFile: parameter when you mounted it) to the `sources/` folder overwriting the existing boot.wim.**   * It is a good idea to backup the original `boot.wim` before replacing it. *

-----

## Common Issues and Solutions ##
### Error: "Failure when attempting to copy boot files" - How to fix ###

This error occurs for a few reasons.  

*First, try to use `diskpart` to make disk0 active and retry the last step:*
   1. type `diskpart` and hit enter to launch the `diskpart` tool.
   2. type `select disk 0` and hit enter to select the disk
   3. type `active` to mark the drive as active
   4. type `exit` and hit enter to exit `diskpart`
   5. run the last step again to try and copy the boot files:  type `bcdboot C:\windows` and hit enter


*If that doesn't work, verify that your BIOS is NOT in UEFI mode and/or that UEFI boot is disabled.*  
   1.  Reboot into the BIOS Setup
   2.  Verify that UEFI is disabled
   3.  Save settings and reboot to the bootable Windows PE USB.  
   4.  Once the script has launched, ***DO NOT*** run the whole imaging process again, instead just retry the last step by typing `bcdboot C:\windows` and hitting enter.  
   5.  You should see a message, "**Boot files successfully created**." if the imaging process is complete.
   6.  Reboot to the first hard disk and finish the Windows setup

-----

## Who do I talk to? ##

* Bryan Karaffa (bryan@geog dot ucsb.edu)
* UCSB Geography IT (support@geog dot ucsb.edu)
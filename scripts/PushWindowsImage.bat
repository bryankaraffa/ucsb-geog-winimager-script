@echo off
set SCRIPT_DIR=X:/ucsb-geog-winimager-script/scripts/
echo =========================================================================
echo Starting the Push Windows Image script.......
echo =========================================================================
set /p myimage=Enter letter of the drive labeled "Image Files" that contains myimage.wim (E):  
echo Running Diskpart preparation with the DiskPartScript...
echo diskpart /s %SCRIPT_DIR%diskpart/DiskPartScript.txt
diskpart /s %SCRIPT_DIR%diskpart/DiskPartScript.txt
echo =========================================================================
echo Running ImageX /apply using %myimage%:\myimage.wim
echo IMAGEX /apply %myimage%:\myimage.wim 1 C:
IMAGEX /apply %myimage%:\myimage.wim 1 C:
echo =========================================================================
echo Finalizing the image and enabling Windows to boot.
bcdboot C:\windows /l en-us
echo =========================================================================
echo Windows Imager Script COMPLETED!! 
echo Please verify that there were no errors and go ahead and reboot.
echo =========================================================================
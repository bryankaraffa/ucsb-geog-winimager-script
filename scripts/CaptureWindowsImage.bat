set timestamp=%date:~10%-%date:~4,2%-%date:~7,2%_%time:~0,2%-%time:~3,2%
@echo off
set SCRIPT_DIR=%~d0%~p0
echo ==============================
echo Starting the script....
echo ==============================
echo WARNING: This script assume that Windows drive letter is "C" - verify this is correct before going forward.
echo Press CTRL+C to exit this script if you are unsure of what to do
echo 
set /p myimage=Enter USB Drive Letter to store the image on (E):    
echo Running imagex.exe /capture
X:\Windows\System32\IMAGEX.EXE /capture C: %myimage%:\myimage_%timestamp%.wim "UCSB Geography Windows 7 (64bit) Image - %timestamp%" /compress maximum /verify
echo ==============================
echo Image created and saved to USB Drive successfully! (%myimage%:\myimage_%timestamp%.wim)
echo ==============================
echo Don't forget to rename the Windows Image file [.wim] to myimage.wim if you wish to use it to push to computers with the PushWindowsImage.bat Script
echo ==============================
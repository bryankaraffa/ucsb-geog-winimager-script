@ECHO OFF
wpeinit
echo =========================================================================
echo  UCSB Geography Windows Imager USB
echo  README and source code available online:
echo  http://bitbucket.org/bryankaraffa/ucsb-geog-winimager-script
echo =========================================================================
echo                         !!!   WARNING   !!!
echo        DO NOT CONTINUE UNLESS YOU KNOW HOW TO USE THIS SCRIPT
echo =========================================================================
echo Preparing volume letters and printing 'diskpart list vol'....
diskpart /S X:\ucsb-geog-winimager-script\scripts\diskpart\PrepareVolumeLetters.txt >"preparevolumeletters.txt"
diskpart /S X:\ucsb-geog-winimager-script\scripts\diskpart\ListVolumes.txt >"volumes.txt"
findstr "Volume" volumes.txt
echo **   Please identify the letter for the volume labeled "Image Files"   **
echo **               You will need it in the next step                     **
echo =========================================================================
echo  You can start the imaging process by running:
echo            X:\PushImage.bat
echo =========================================================================